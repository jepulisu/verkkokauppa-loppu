<?php include_once 'inc/top.php'; ?>
<div class="row">
    <div class="col-xs-6">      
        <div class="dropdown">
            <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Tuotteet
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <?php
                $sqlmm='SELECT * FROM tuoteryhma';
                $linkki = $_SERVER['PHP_SELF'];
                
                $kyselytmm=$tietokantamm->query($sqlmm);
                $kyselytmm->setFetchMode(PDO::FETCH_OBJ);                
                               
                while($tietue = $kyselytmm->fetch()) {
                    print '<li>';
                    print "<a href='index.php?id=$tietue->id'>"; // GET parametrina lähetys
                    print $tietue->nimi;
                    print '</a><li>';
                    
                }
                ?>
            </ul>
        </div>       
    </div>
    <div class="col-xs-6" style="text-align: right;">
        <button class="btn btn-lg btn-info" data-toggle="collapse" data-target="#ostoskori">
            <span class="glyphicon glyphicon-shopping-cart ostoskorinappi"> 100.00 eur</span>
        </button>
    </div>
    <div class="col-xs-12" style="text-align: right;">
        <div id="ostoskori" class="collapse ostoskori">
            <table>
                <?php
                
                var_dump($_SESSION);
                if (is_array($_SESSION['kori'])){                               
                    $laskuri = count($_SESSION['kori']);
                    for($i=0;$i<$laskuri;$i+=2){                    
                    print "<tr>";                    
                    print "<td>". $_SESSION['kori'][$i] . "</td>";
                    
                    print "<td>". $_SESSION['kori'][$i+1] . "</td>";                              
                    print '<td><span class="glyphicon glyphicon-trash"></span></td>';
                    print "</tr>";
                    }
                }
                
                ?>
                <tr class="summa">
                    <td>Summa</td>
                    <?php if (is_array($_SESSION['kori'])){
                        $tulos=0;
                        for($i=1;$i<$laskuri;$i+=2) {
                            $tulos +=$_SESSION['kori'][$i];
                        }
                        print "<td>";
                        print $tulos . " € ";
                        print "</td>";
                    }
                    ?>
                    
                    <td></td>
                </tr>
            </table>
            <a href="order.php" class="btn btn-primary">
                Kassalle
            </a>
            <a class="btn btn-default" href="<?php print $_SERVER['PHP_SELF']?>?poista=true">
                Tyhjennä
            </a>
        </div>
    </div>
</div>
<div class="row">             
    <div class="col-xs-12 tuotteet">
            <div class="row">
            <div class="col-xs-12">                           
            <h4>Kengät</h4>
            <hr></div></div>
            <div class="row">
            <?php 
            $sql= 'SELECT * FROM tuote';
            if($_SERVER['REQUEST_METHOD']== "GET") {
                    if(isset($_GET['id']) && !empty($_GET['id'])){
                    $idmm = $_GET['id'];
                    $sql='SELECT * FROM tuote WHERE tuoteryhma_ID='.$idmm;
                    }

                }
//            $sql='SELECT * FROM tuote WHERE tuoteryhma_ID=4';
            
            $kyselymm=$tietokantamm->query($sql);
            $kyselymm->setFetchMode(PDO::FETCH_OBJ);
            

            while($tietue = $kyselymm->fetch()) {
            
            
                print '<div class="col-xs-6 col-sm-4 col-md-3 tuote">';
                print '<p>'."<a href='tuotesivu.php?id=$tietue->id'>";
                print '<img  width="100" height="100" src=tuotteet/';
                print $tietue->kuva;
                print '>'.'</a>';
                print '<br />';
                print $tietue->nimi;
                print '<br />';
                print $tietue->hinta;
                print '</p>';
                print '</div>';
            }
            ?>            
        </div>
</div>
</div>
<?php include_once 'inc/bottom.php'; ?>