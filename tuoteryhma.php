<?php include_once 'inc/top.php';?>
<?php
if ($_SERVER['REQUEST_METHOD']=="POST"){
    $nimi = filter_input(INPUT_POST,"tuoteRyhma",FILTER_SANITIZE_STRING);
    $tietokantamm->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    
    $kyselymm = $tietokantamm->prepare("INSERT INTO tuoteryhma (nimi)" . "VALUES(:nimi)");
    
    $kyselymm->bindvalue(":nimi",$nimi,PDO::PARAM_STR);
    $kyselymm->execute();
    print "<p>Onnistui</p>";
    print "<a href='index.php'>Etusivulle</a>";
    
}

?>
<h2>Lisää tuoteryhmä</h2>
<form method="post" action="<?php print $_SERVER['PHP_SELF'];?>">
  <div class="form-group">
    <label for="tuoteRyhma">Nimi</label>
    <input type="text" class="form-control" id="tuoteRyhma" name="tuoteRyhma"placeholder="Tuoteryhma">
  </div>
    <button type="submit" class="btn btn-primary">Tallenna</button>
    <button type="" class="btn">Peruuta</button>
</form>
<?php include_once 'inc/bottom.php';?>