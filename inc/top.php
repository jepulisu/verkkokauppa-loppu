<?php
session_start();
session_regenerate_id();
if(!isset($_SESSION['kori'])) {
    $_SESSION['kori'] = array();
}

if (isset($_GET['poista'])) {
    unset($_SESSION['kori']);
    $laskuri = 0;
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <title></title>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
		</button>        
              <a class="navbar-brand" href="index.php">Verkkokauppa</a>
              <ul class="nav navbar-nav">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ylläpito <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                          <li><a href="tuoteryhma.php">Lisää tuoteryhmä</a></li>
                          <li><a href="tuote.php">Lisää tuote</a></li>   
                      </ul>
                  </li>
              </ul>
            </div>   
          </div>
        </nav>
        <div class="container content">
            <div class="row">
                <div class="col-sm-12">
                                 
                
<?php
try {
    $tietokantamm = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8', 'root','');
    
    
} catch (PDOException $pdoex) {
    print "Häiriö tietokannassa <br>" . $pdoex->getMessage();

}

?>