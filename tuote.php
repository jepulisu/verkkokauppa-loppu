<?php include_once 'inc/top.php';?>

<?php
if ($_SERVER['REQUEST_METHOD']=="POST"){
    
    if ($_FILES['kuva']['error'] == UPLOAD_ERR_OK)
    {
        $tiedostomm = $_FILES['kuva']['name'];
        
        if ($_FILES['kuva']['size'] > 0)
        {
            $tyyppimm=$_FILES['kuva']['type'];
            if (strcmp($tyyppimm, "image/jpg")==0 || strcmp($tyyppimm,"image/png")==0 || strcmp($tyyppimm,"image/jpeg")==0)
            {
                $tiedostomm = basename($tiedostomm);
                $kansiomm='tuotteet/';
                move_uploaded_file($_FILES['kuva']['tmp_name'],"$kansiomm/$tiedostomm");
                
            }
        }
        
    $nimimm = filter_input(INPUT_POST,"tuote",FILTER_SANITIZE_STRING);
    $kuvausmm = filter_input(INPUT_POST,"kuvaus",FILTER_SANITIZE_STRING);
    $hintamm = filter_input(INPUT_POST,"hinta",FILTER_SANITIZE_STRING);
    $tuoteryhmamm = filter_input(INPUT_POST,"tuoteryhma",FILTER_SANITIZE_STRING);
    
    $tietokantamm->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    
    $kyselymm = $tietokantamm->prepare("INSERT INTO tuote (nimi,kuvaus,hinta,kuva,tuoteryhma_id)" 
            . "VALUES(:nimi,:kuvaus,:hinta,:kuva,:tuoteryhma_id)");
    
    $kyselymm->bindvalue(":nimi",$nimimm,PDO::PARAM_STR);
    $kyselymm->bindvalue(":kuvaus",$kuvausmm,PDO::PARAM_STR);
    $kyselymm->bindvalue(":hinta",$hintamm,PDO::PARAM_STR);
    $kyselymm->bindvalue(":kuva",$tiedostomm,PDO::PARAM_STR);
    $kyselymm->bindvalue(":tuoteryhma_id",$tuoteryhmamm,PDO::PARAM_STR);
    $kyselymm->execute();    
    print "<p>Onnistui</p>";
    print "<a href='index.php'>Etusivulle</a>";
    }
}

?>

<h2>Lisää tuote</h2>
<form method="post" action="<?php print $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
    <div class="form-group">
    <label for="tuoteryhma">Tuoteryhmä</label>
    <select class="form-control" id="tuoteryhma" name="tuoteryhma">
        <?php
        $sql='SELECT * FROM tuoteryhma';
        
        $kyselymm=$tietokantamm->query($sql);
        $kyselymm->setFetchMode(PDO::FETCH_OBJ);
        
        while ($tietue = $kyselymm->fetch()) {
            print '<option value="'.$tietue->id.'">';
            
            print '';
            print $tietue->nimi;
            print '</option>';
        }        
        ?>
    </select>
    </div>
    <div class="form-group">
      <label for="tuote">Nimi</label>
      <input type="text" class="form-control" id="tuote" name="tuote"placeholder="Tuotteen nimi">
    </div>
    <div class="form-group">
        <label for="kuvaus">Kuvaus</label>
        <textarea class="form-control" rows="3" name="kuvaus" id="kuvaus"></textarea>
    </div>
    <div class="form-group">
        <label for="kuva">Kuvatiedosto</label>
        <input type="file" id="kuva" name="kuva">
    </div>
    <div class="form-group">
        <label for="hinta">Hinta</label>
        <input type="number" class="form-control" id="hinta" name="hinta" step="1">
    </div>
    <button type="submit" class="btn btn-primary">Lähetä</button>
    <button type="" class="btn">Peruuta</button>
</form>
<?php include_once 'inc/bottom.php';?>
